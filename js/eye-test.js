var arMonitorSize = [15, 17, 18.5, 19, 21, 21.5, 27];
var arResolution = ['800x600', '1024x768', '1024x600', '1152x768', '1280x720', '1280x768', '1280x800', '1280x854', '1280x960', '1280x1024', '1366x768', '1400x1050', '1440x900', '1440x960', '1600x1200', '1920x1080', '2048x1536'];
var xDPI = screen.logicalXDPI || screen.deviceXDPI, yDPI = screen.logicalYDPI || screen.deviceYDPI;
var PPmm = xCorrection = yCorrection = 0; // Pixels Per mm

if (!xDPI) {
    var el = $('<div style="width:1in;height:1in;position:fixed;top:-2in;left:-2in;"></div>').appendTo('body');
    xDPI = el[0].offsetWidth, yDPI = el[0].offsetHeight;
}

if (!xDPI) xDPI = yDPI = 96;
var screenWidth = screen.width / xDPI, screenHeight = screen.height / yDPI;
var approxMonitorSize = Math.sqrt((screenWidth * screenWidth) + (screenHeight * screenHeight));
var diff = 100, monitorSizeInInch = 19;
for (var i in arMonitorSize) {
    var newDiff = Math.abs(approxMonitorSize - arMonitorSize[i]);
    if (newDiff > diff) break;
    monitorSizeInInch = arMonitorSize[i];
    diff = newDiff;
}

var selResolution = $('#eye-test-selResolution');
var resolution = screen.width + 'x' + screen.height;
if ($.inArray(resolution, arResolution) === -1) selResolution.append('<option>' + resolution + '</option>');

selResolution.val(resolution);

$('#eye-test-btnAddResolution').click(function () {
    var res;
    if (res = prompt('اضف دقة الشاشة')) {
        res = res.toLowerCase();

        var tmp = res.split('x');
        if (tmp.length === 2 && tmp[0] && tmp[1] && !isNaN(tmp[0]) && !isNaN(tmp[1])) {
            if ($.inArray(res, arResolution) === -1) {
                selResolution.append('<option>' + res + '</option>');
            }
            selResolution.val(res);
        } else {
            alert('دقة الشاشة غير سليمة. دقة الشاشة يجب ان تكون ضمن صيغة طولxعرض. مثال  1024x768');
        }
    }
    return false;
});


var selMonitorSize = $('#eye-test-selMonitorSize');

selMonitorSize.val(monitorSizeInInch);
$('#eye-test-btnAddMonitorSize').click(function () {
    var size;
    if (size = prompt('ادخل الحجم الجديد للشاشة')) {
        if (!isNaN(size) && size > 15 && size < 35) {
            if ($.inArray(size, arMonitorSize) === -1) {
                selMonitorSize.append('<option value="' + size + '">' + size + '"</option>');
            }
            selMonitorSize.val(size);
        } else {
            alert('حجم الشاشة غير سليم. الرجاء ادخال رقم بين الـ 15-35');
        }
    }
    return false;
});

var sizeBlock = $('#eye-test-sizeBlock');

var letter, currentRotation;
var arResult = [60, 30, 21, 15, 12, 9, 7.5, 6];
var arResultText = ['حدة البصر والرؤية 60/6 (200/20) تعتبر ضعف بصري حاد. عندما تكون حدة الرؤية القصوى 60/6 او اسوأ، فان الشخص يعتبر "ضريرا" بحسب القانون.',
    '30/6 (100/20) تعني انك عندما تقف على بعد 6 أمتار عن اللوح بامكانك ان ترى ما يستطيع شخص عادي رؤيته من مسافة 30 متر. حدة الرؤية 30/6 يقدر على انه ضعف بصري معتدل. ',
    'عندما تكون حدة البصر لديك 21/6 (70/20) معنى ذلك- كي تستطيع رؤية ما يراه شخص يملك رؤية سليمة من مسافة 21 متر (70 قدماً) يجب عليك ان تكون على مسافة 6 أمتار (20 قدم). رؤية 21/6 (70/20) مصنفة على انها ضعف بصري قوي / محدود الرؤية. ',
    ' يجب عليك ان تكون على قرابة 6 أمتار (20 قدماً) لكي تستطيع رؤية ما يراه شخص مع رؤية سليمة من 15 متراً (50 قدماً). رؤية 15/6 50/20 تعتبر فقدان معتدل / طفيف للبصر او رؤية شبه سليمة. ',
    'معنى هذا انه يجب ان تكون على مسافة 6 أمتار (20 قدم) لكي ترى ما يراه شخص رؤيته سليمة من 12 متراً (40 قدم)، ينصح باستشارة اخصائي عيون. ',
    'رؤية 9/6 (30/20) مصنفة على انها فقدان معتدل للرؤية، قد تواجه ربما صعوبة في القراءة، اذا كان يضايقك الامر استعمل النظارات. ',
    'رؤيتك شبه ممتازة',
    'رؤيتك ممتازة'];

var arInstructions = [
'<p class="alert alert-info">اختر دقة ووضوح الشاشة (resolution) لجهازك وحجم الشاشة من الاختيارات المتاحة في الاسفل. اذا لم تتواجد الاحجام والعيارات المناسبة لدقة الشاشة في اللائحة المعطاه ادناه، انقر على روابط ، اضف حجم الشاشة /اضف دقة الشاشة ، لاضافة المعلومات والتفاصيل عن شاشتك.</p>',
'<p class="alert alert-info">من اجل التأكد من صحة الاحجام وعيارات الشاشة التي زودتها، قم بقياس حجم الصندوق الاسود في الاسفل بواسطة مسطرة.</p>',
'<p class="alert alert-info">احجب عينك اليسرى وانظر بعينك اليمنى الى الحرف E المعطى في الاسفل. انقر على السهم الذي يطابق اتجاه اطراف حرف ال E. اي، اذا كانت اطراف الحرف تواجه الجهة اليمنى اضغط اذا على السهم اليميني، اذا كان الحرف يشير الى الاسفل انقر اذا على السهم السفلي وهكذا. حالما تنهي الفحص في عينك اليمنى، احجب  اليمنى وكرر الفحص في عينك اليسرى المفتوحة. </p>',
'<p class="alert alert-info">لفحص حدة رؤيتك, ابدا الفحص وحاول تشخيص الجهة التي تشير اليها اطراف الحرف E بواسطة الضغط على السهم الذي يتلائم مع جهة الحرف.</p>'
];

var panels = $('.eye-test-testWrapper .panel');

$('#eye-test-aboutTest').fadeIn();

$('#eye-test-startETest').click(function () {
    $('.panel').hide();
    showPara(0);
    $('.eye-test-testWrapper, #eye-test-screenSizeWrapper').fadeIn();
    return false;
});

$('#eye-test-btnShowSizeBlock').click(function () {
    var resolution = selResolution.val().split('x');
    var monitorSizeInInch = selMonitorSize.val();
    showPara(1);

    var monitorSize = monitorSizeInInch * 25.4;
    
    PPmm = Math.sqrt((resolution[0] * resolution[0] + resolution[1] * resolution[1])) / monitorSize;

    sizeBlock.css({ width: 50 * PPmm, height: 50 * PPmm });
    panels.hide();
    $('#eye-test-aboutTest').hide();
    $('#eye-test-confirmSize').fadeIn();
    return false;
});

$('#eye-test-btnPrevious').click(function () {
    panels.hide();
    showPara(0);
    $('.eye-test-testWrapper, #eye-test-screenSizeWrapper').fadeIn();
    return false;
});

$('#eye-test-btnShowInfo').click(function (ev) {
    var resolution = selResolution.val().split('x');
    var monitorSizeInInch = selMonitorSize.val();

    var monitorSize = monitorSizeInInch * 25.4;

    PPmm = Math.sqrt((resolution[0] * resolution[0] + resolution[1] * resolution[1])) / monitorSize; // 

    showPara();
    var sizeBlockWidth = 50;
    var sizeBlockHeight = 50;
    if (isNaN(sizeBlockWidth) || !sizeBlockWidth || isNaN(sizeBlockHeight) || !sizeBlockHeight) {
        alert('بعد غير سليم. الرجاء فحص معطياتك');
        return false;
    }
    xCorrection = 50 / sizeBlockWidth;
    yCorrection = 50 / sizeBlockHeight;
    panels.hide();
    $('#eye-test-infoMessage').fadeIn();
    ev.preventDefault();
    return false;
});
$('#eye-test-btnStartTest').click(function () {

    showPara(2);
    panels.hide();
    $('#eye-test-chartImg').fadeIn();
    $('#eye-test-pchartImg').fadeIn();
    var letterSize = (PPmm * 88) / 6.096;


    var paper = new Raphael(document.getElementById('eye-test-canvasContainer'), letterSize + 100, letterSize + 100);
    var lineWidth = letterSize / 5;
    var cornerPos = [letterSize + 50, lineWidth, lineWidth * 4, lineWidth * 5];

    letter = paper.path('M ' + cornerPos[0] + ' 50 l -' + cornerPos[3] + ' 0 l 0 ' + cornerPos[3] + ' l ' + cornerPos[3] + ' 0 l 0 -' + cornerPos[1] + ' l -' + cornerPos[2] + ' 0 l 0 -' + cornerPos[1] + ' l ' + cornerPos[2] + ' 0 l 0 -' + cornerPos[1] + ' l -' + cornerPos[2] + ' 0 l 0 -' + cornerPos[1] + ' l ' + cornerPos[2] + ' 0 z');
    letter.attr({ 'stroke-width': 0, 'stroke-opacity': 0, fill: '#000' });
    letter.scale(xCorrection, yCorrection);


    currentRotation = Math.floor(Math.random() * 4) * 90;
    letter.rotate(-currentRotation);
    return false;
});

var idx = size = errorCount = 0;

var lstAnchor = $('#eye-test-chartImg a').click(function (ev) {
    ev.preventDefault();
    idx++;
    var pos = lstAnchor.index(this);
    var degree = $(this).attr('data-degree');
    if (degree != currentRotation) {
        errorCount++;
    }
    if (idx == 4 && errorCount == 1) {
        // One more try
    } else if (idx >= 4) {
        size++;

        if ((size >= arResult.length) || (errorCount >= 2)) {
            showPara();
            $('.eye-test-result').show();
            $('.eye-test-testWrapper').hide();
            $('#eye-test-resultContainer').html('نتائج الفحص: رؤيتك هي 6 / ' + arResult[size - 1] + '<br/>' + arResultText[size - 1]).parent().fadeIn();
            $('#eye-test-startover').show();
            return;
        } else {
            idx = 0;
            errorCount = 0;
            letter.scale(arResult[size] / arResult[size - 1]);
        }
    }
    
    do {
        var randomAngle = Math.floor(Math.random() * 4) * 90;
    } while (currentRotation - randomAngle == 0);

    letter.rotate(currentRotation - randomAngle);
    currentRotation = randomAngle;
    return false;
});

function showPara(para, selector, insertPos) {

    var ud = 'undefined';
    $('.eye-test-instructionHolder').find('p, div').hide();
    $('.eye-test-paraPanel').hide();
    if (!isNaN(para) || $.isArray(para)) {
        if ($.isArray(para)) {
            var arLen = para.length;
            for (var i = 0; i <= arLen; i++) {
                $(arInstructions[para[i]]).appendTo('.eye-test-instructionHolder').fadeIn();
            }
        } else {
            $(arInstructions[para]).appendTo('.eye-test-instructionHolder').fadeIn();
        }
    } else if (typeof selector !== ud && selector !== '') {
        if (typeof insertPos !== ud || insertPos !== '') {
            if (insertPos === 'after')
                $(para).insertAfter(selector).fadeIn();
            else
                $(para).insertBefore(selector).fadeIn();
        } else {
            $(para).appendTo(selector).fadeIn();
        }
    }
}